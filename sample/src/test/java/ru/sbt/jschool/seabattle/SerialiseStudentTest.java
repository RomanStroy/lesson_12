package ru.sbt.jschool.seabattle;

import org.junit.Assert;
import org.junit.Test;

import java.io.*;

/**
 * Created by roma on 02.04.2019.
 */

public class SerialiseStudentTest {

    private User createUser() {
        School school = new School();
        school.setAddress("Улица Ленина д.1");
        school.setName("Школа 1");

        Student student = new Student("");
        student.setSchool(school);
        student.setName("Иванов Иван");
        student.setDocumentNum("11");

        return student;
    }


    @Test
    public void testSerizliseStudent() throws IOException, ClassNotFoundException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        User user = createUser();

        oos.writeObject(user);

        ObjectInputStream bis = new ObjectInputStream(new ByteArrayInputStream(bos.toByteArray()));
        User userClone = (User) bis.readObject();

        try (FileOutputStream fos = new FileOutputStream("serializedUser.txt");) {
            fos.write(bos.toByteArray());
        }

        Assert.assertEquals(user, userClone);
        Assert.assertTrue(user != userClone);


    }

    @Test
    public void testSerizliseStudentDeserializeRenamed() throws IOException, ClassNotFoundException {
        String fileName = "serializedUserOriginal.txt";
        User user = createUser();

        try (
                FileOutputStream fos = new FileOutputStream(fileName);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
        ) {

            oos.writeObject(user);
        }


        try (
                FileInputStream fis = new FileInputStream(fileName);
                ObjectInputStream ois = new ObjectInputStream(fis);
        ) {
            User userClone = (Student) ois.readObject();
            Assert.assertEquals(user, userClone);
            Assert.assertTrue(user != userClone);
        }

    }
}
