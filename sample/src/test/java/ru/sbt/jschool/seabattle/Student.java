package ru.sbt.jschool.seabattle;

import java.io.ObjectStreamField;
import java.io.Serializable;

/**
 * Created by roma on 02.04.2019.
 */
public class Student extends User implements Serializable {
//    private static final long serialVersionUID = 1;

    private static final ObjectStreamField[] serialPersistentFields = {
        new ObjectStreamField("documentNum", String.class),
        new ObjectStreamField("school", School.class),
    };

    private School school;
    private String documentNum;


    public Student() {
    }

    public Student(String name) {
        super(name);
    }

    public String getDocumentNum() {
        return documentNum;
    }

    public void setDocumentNum(String documentNum) {
        this.documentNum = documentNum;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;

        Student student = (Student) o;

        if (getSchool() != null ? !getSchool().equals(student.getSchool()) : student.getSchool() != null) return false;
        return getDocumentNum() != null ? getDocumentNum().equals(student.getDocumentNum()) : student.getDocumentNum() == null;

    }

    @Override
    public int hashCode() {
        int result = getSchool() != null ? getSchool().hashCode() : 0;
        result = 31 * result + (getDocumentNum() != null ? getDocumentNum().hashCode() : 0);
        return result;
    }
}
